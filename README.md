# vue点击列表项背景颜色改变
本项目使用vue实现：点击列表项时，被点击的列表项颜色改变的功能；

### 运用的知识点：

- vue的v-for实现数组的遍历

- vue的v-on:click实现点击事件

- 原生JS的getAttribute()函数实现元素属性值的获取

- 原生JS的setAttribute()函数实现元素属性值的修改与属性的添加

- 使用querySelectorAll()函数获取dom元素

- e.currentTarget  //获取绑定事件的元素

- e.target  //表示当前的点击的元素

### 实现的思路：

- 首先将遍历的列表项渲染到页面中，并给每一个列表项设置一个属性值flagID=0

- 并且给每一个列表项添加一个click事件

- 点击列表项时，判断当前列表项的flagID属性值是否为0，如果为0，将当前的列表项的flagID属性值置为1，并且修改此列表项的背景颜色；并且在修改前将所有的列表项的flagID属性值初始化成0，并修改成原始的背景颜色
